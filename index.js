/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	let yourName = prompt("Enter your name");
	console.log("Hello, " + yourName);
	let yourAge = prompt("How old are you?");
	console.log("You are " + yourAge + " years old.");
	let yourLocation = prompt("Where do you live?");
	console.log("You live in " + yourLocation);
	alert("Thank you for your input!");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBand() {
		
		let Band = ["Cueshe", "Moonstar88", "MYMP", "Kiss Jane", "Rivermaya"]
		console.log(Band);
	};

	favoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovies() {
		let oneMovie = "1. The Hitman's Bodyguard";
		let twoMovie = "2. The Hitman's Wife's Bodyguard";
		let threeMovie = "3. Army of Theives";
		let fourMovie = "4. The Old Guard";
		let fiveMovie = "5. Uncharted";

		console.log(oneMovie);
		console.log("Rotten Tomatoes Rating: 97%");
		console.log(twoMovie);
		console.log("Rotten Tomatoes Rating: 96%");
		console.log(threeMovie);
		console.log("Rotten Tomatoes Rating: 91%");
		console.log(fourMovie);
		console.log("Rotten Tomatoes Rating: 93%");
		console.log(fiveMovie);
		console.log("Rotten Tomatoes Rating: 96%");
	}

	favoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

printUsers();
	function printUsers() /* = function printUsers()*/{
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


